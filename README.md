

# 公經中心

## INSTALLATION & SETTINGS

### Install Django

To install django, just type the following command

    sudo pip install django
    
### Create Django project from the template

To create the project, run the following command and please replace your\_project_name to what you like :

	git clone https://utlai@bitbucket.org/dorm7/ntu.git

### Setting Virtualenv

At first, you should make sure you have [virtualenv](http://www.virtualenv.org/) installed. 

after that, just cd to your\_project_name:

   cd your\_project_name

Then create your virtualenv:

    virtualenv venv
    
Second, you need to enable the virtualenv by

	source venv/bin/activate
	



### Install requirements

For development:

    pip install -r requirements/local.txt

For production:

    pip install -r requirements.txt

For heroku:

Use requirements-heroku.txt to replace requirements.txt


### Start To Run

to start project.  cd your\_project_name again (find manage.py )

```bash
python manage.py migrate   # you just do it once

python manage.py runserver
```




	