import os

from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtailsearch import urls as wagtailsearch_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls

import style.views

urlpatterns = patterns(
    '',
    url(r'^django-admin/', include(admin.site.urls)),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),

    url(r'^style_guide/(?P<section>\d*)$', style.views.StyleGuideView.as_view(), name='styleguide'),

    #mockups
    url(r'mockups/homepage/', TemplateView.as_view(template_name='mockups/homepage.html')),
    url(r'mockups/topics/', TemplateView.as_view(template_name='mockups/topics.html')),
    url(r'mockups/article/', TemplateView.as_view(template_name='mockups/article.html')),

    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'', include(wagtail_urls)),
    url(r'^search/', include(wagtailsearch_urls)),
)




if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL + 'images/', document_root=os.path.join(settings.MEDIA_ROOT, 'images'))



