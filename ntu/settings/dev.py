from .base import *

DEBUG = True
TEMPLATE_DEBUG = True


#  Django Compressor for development. so it can put image to correct place
COMPRESS_ENABLED = True
COMPRESS_REBUILD_TIMEOUT = 0

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    from .local import *
except ImportError:
    pass
