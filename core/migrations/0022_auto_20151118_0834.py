# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20151118_0657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogindexpage',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='title[en]', blank=True),
            preserve_default=True,
        ),
    ]
