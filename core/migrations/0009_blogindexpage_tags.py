# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import modelcluster.tags


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        ('core', '0008_auto_20141116_1051'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexpage',
            name='tags',
            field=modelcluster.tags.ClusterTaggableManager(to='taggit.Tag', through='core.BlogPageTag', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
            preserve_default=True,
        ),
    ]
