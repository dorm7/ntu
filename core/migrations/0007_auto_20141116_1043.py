# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0004_make_focal_point_key_not_nullable'),
        ('core', '0006_blogindexpage'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexpage',
            name='image',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wagtailimages.Image', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogindexpage',
            name='intro',
            field=wagtail.wagtailcore.fields.RichTextField(default='', blank=True),
            preserve_default=False,
        ),
    ]
