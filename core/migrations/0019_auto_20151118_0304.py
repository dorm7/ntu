# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_auto_20151118_0302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='title_en',
            field=wagtail.wagtailcore.fields.RichTextField(default=b'', verbose_name='title[en]'),
            preserve_default=True,
        ),
    ]
