# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_homepagecarouselitem_caption_en'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpage',
            name='body_en',
            field=wagtail.wagtailcore.fields.RichTextField(default=b'', verbose_name='\u5167\u6587[en]'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpage',
            name='subheading_en',
            field=models.CharField(default=b'', max_length=512, verbose_name='\u526f\u6a19\u984c[en]', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpage',
            name='title_en',
            field=wagtail.wagtailcore.fields.RichTextField(default=b'', verbose_name='\u5167\u6587'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='homepagecarouselitem',
            name='caption_en',
            field=models.CharField(max_length=255, verbose_name='\u6a19\u984c[en]', blank=True),
            preserve_default=True,
        ),
    ]
