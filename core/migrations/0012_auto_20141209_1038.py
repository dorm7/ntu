# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_blogpage_category'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blogindexpage',
            options={'verbose_name': '\u8b70\u984c\u9801'},
        ),
        migrations.AddField(
            model_name='blogpage',
            name='co_host',
            field=models.CharField(default=b'', max_length=1024, verbose_name='\u7814\u8a0e\u6703\u8207\u8ac7\u4eba', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpage',
            name='conference_date',
            field=models.DateField(default=datetime.datetime(2014, 12, 9, 10, 38, 3, 111470, tzinfo=utc), verbose_name='\u7814\u8a0e\u6703\u65e5\u671f', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='blogpage',
            name='host',
            field=models.CharField(default=b'', max_length=1024, verbose_name='\u7814\u8a0e\u6703\u4e3b\u6301\u4eba', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='date',
            field=models.DateField(verbose_name='\u767c\u5e03\u65e5\u671f'),
            preserve_default=True,
        ),
    ]
