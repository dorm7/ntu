# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_blogindexpage_intro_en'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexpage',
            name='title_en',
            field=wagtail.wagtailcore.fields.RichTextField(verbose_name=b'title[en]', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='homepage',
            name='body_en',
            field=wagtail.wagtailcore.fields.RichTextField(verbose_name=b'body_en', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='homepage',
            name='title_en',
            field=wagtail.wagtailcore.fields.RichTextField(default=b'', verbose_name='title[en]'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogindexpage',
            name='intro_en',
            field=wagtail.wagtailcore.fields.RichTextField(verbose_name=b'intro[en]', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='title_en',
            field=models.CharField(help_text="The page title as you'd like it to be seen by the public", max_length=255, verbose_name='title[en]'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.wagtailcore.fields.RichTextField(verbose_name=b'body', blank=True),
            preserve_default=True,
        ),
    ]
