# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_remove_homepagecarouselitem_embed_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpage',
            name='subheading',
            field=models.CharField(default=b'', max_length=512, verbose_name='\u526f\u6a19\u984c', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpagerelatedlink',
            name='link_external',
            field=models.URLField(verbose_name='\u5916\u90e8\u9023\u7d50', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='homepagecarouselitem',
            name='caption',
            field=models.CharField(max_length=255, verbose_name='\u6a19\u984c', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='homepagecarouselitem',
            name='link_external',
            field=models.URLField(verbose_name='\u5916\u90e8\u9023\u7d50', blank=True),
            preserve_default=True,
        ),
    ]
