# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_blogpage_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='conference_date',
            field=models.DateField(null=True, verbose_name='\u7814\u8a0e\u6703\u65e5\u671f', blank=True),
            preserve_default=True,
        ),
    ]
