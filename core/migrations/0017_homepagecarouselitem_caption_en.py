# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20141220_0544'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepagecarouselitem',
            name='caption_en',
            field=models.CharField(max_length=255, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
