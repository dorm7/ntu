# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20151118_0834'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='title_en',
            field=models.CharField(help_text="The page title as you'd like it to be seen by the public", max_length=255, verbose_name='title[en]', blank=True),
            preserve_default=True,
        ),
    ]
