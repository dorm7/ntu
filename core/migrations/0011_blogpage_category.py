# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20141203_0919'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpage',
            name='category',
            field=models.CharField(default=b'B', max_length=2, verbose_name='\u985e\u578b', choices=[(b'B', '\u4e00\u822c\u6587\u7ae0'), (b'C', '\u7814\u8a0e\u6703'), (b'I', '\u570b\u969b\u4ea4\u6d41')]),
            preserve_default=True,
        ),
    ]
