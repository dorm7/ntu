# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
    PageChooserPanel, MultiFieldPanel, InlinePanel, )
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager

from taggit.models import Tag, TaggedItemBase
from django.utils import translation


class TranslatedField(object):
    def __init__(self, field, en_field):
        self.field = field
        self.en_field = en_field

    def __get__(self, instance, owner):
        zh = getattr(instance, self.field)
        en = getattr(instance, self.en_field)

        if translation.get_language() == 'en':
            if not en:
                return zh
            return en
        else:
            return zh


class LinkFields(models.Model):
    link_external = models.URLField(u"外部連結", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    class Meta:
        abstract = True


class CarouselItem(LinkFields):

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    caption = models.CharField(u'標題', max_length=255, blank=True)
    caption_en = models.CharField(u'標題[en]', max_length=255, blank=True)

    translated_caption = TranslatedField('caption', 'caption_en')

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
        FieldPanel('caption_en'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True
        verbose_name = u'首頁聯播'


class HomePageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('HomePage', related_name='carousel_items')



class HomePage(Page):

    body = RichTextField('body', blank=True)
    body_en = RichTextField('body_en', blank=True)
    translated_body = TranslatedField('body', 'body_en')

    title_en = RichTextField(u'title[en]', default='')
    translated_title = TranslatedField('title', 'title_en')

    class Meta:
        verbose_name = u"首頁"

    @property
    def internationals(self):
        internationals = BlogPage.objects.live().filter(category='I').filter(pk__in=self.get_descendants().values_list('pk', flat=True))
        internationals = internationals.order_by('-date')
        return internationals

    @property
    def conferences(self):
        conferences = BlogPage.objects.live().filter(category='C').filter(pk__in=self.get_descendants().values_list('pk', flat=True))
        conferences = conferences.order_by('-conference_date')
        return conferences

    @property
    def carouselItems(self):
        return HomePageCarouselItem.objects.all()

    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        context['blog_index_pages'] = BlogIndexPage.objects.all()
        context['carouselItems'] = self.carouselItems
        context['conferences'] = self.conferences
        context['internationals'] = self.internationals
        return context


HomePage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('title_en', classname="full title"),
    InlinePanel(HomePage, 'carousel_items', label=u"首頁聯播"),
]


class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class BlogPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('BlogPage', related_name='related_links')


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('BlogPage', related_name='tagged_items')


class BlogIndexPageTag(TaggedItemBase):
    content_object = ParentalKey('BlogIndexPage', related_name='tagged_items')


class BlogPage(Page):

    title_en = models.CharField(u'title[en]', max_length=255, blank=True, help_text=_("The page title as you'd like it to be seen by the public"))
    translated_title = TranslatedField('title', 'title_en')

    body = RichTextField(u'內文')
    body_en = RichTextField(u'內文[en]', default='')
    translated_body = TranslatedField('body', 'body_en')

    subheading = models.CharField(u'副標題', max_length=512, default="", blank=True)
    subheading_en = models.CharField(u'副標題[en]', max_length=512, default="", blank=True)
    translated_subheading = TranslatedField('subheading', 'subheading_en')

    date = models.DateField(u'發布日期')
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    host = models.CharField(u'研討會主持人', max_length=1024, blank=True, default="")
    co_host = models.CharField(u'研討會與談人', max_length=1024, blank=True, default="")

    conference_date = models.DateField(u'研討會日期', blank=True, null=True)

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    CATEGORY_CHOICES = (
        ('B', u'一般文章'),
        ('C', u'研討會'),
        ('I', u'國際交流'),
    )

    category = models.CharField(u'類型', max_length=2, choices=CATEGORY_CHOICES, default='B')

    @property
    def related_blogs(self):
        related = self.get_siblings().live().order_by('?')
        pk_list = [blog['pk'] for blog in related.values('pk')]
        related = BlogPage.objects.filter(pk__in=pk_list).exclude(pk=self.pk)
        return related

    def get_context(self, request):
        context = super(BlogPage, self).get_context(request)
        context['related_blogs'] = self.related_blogs[:5]
        return context

    class Meta:
        verbose_name = u'文章'


BlogPage.content_panels = [

    FieldPanel('title', classname='full title'),
    FieldPanel('title_en', classname='full title'),

    FieldPanel('subheading'),
    FieldPanel('subheading_en'),

    FieldPanel('body', classname='full'),
    FieldPanel('body_en', classname='full'),

    ImageChooserPanel('image'),
    FieldPanel('category'),
    FieldPanel('date'),
    FieldPanel('host'),
    FieldPanel('co_host'),
    FieldPanel('conference_date'),
    InlinePanel(BlogPage, 'related_links', label="Related Links")
]

class BlogIndexPage(Page):

    title_en = models.CharField(u'title[en]', max_length=255, blank=True)
    translated_title = TranslatedField('title', 'title_en')

    intro = RichTextField(blank=True)
    intro_en = RichTextField("intro[en]", blank=True)
    translated_intro = TranslatedField('intro', 'intro_en')

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    tags = ClusterTaggableManager(through=BlogIndexPageTag, blank=True)

    @property
    def blogs(self):
        blogs = BlogPage.objects.live().descendant_of(self)
        blogs = blogs.order_by('-date')
        return blogs

    def get_context(self, request):
        blogs = self.blogs
        context = super(BlogIndexPage, self).get_context(request)
        context['blogs'] = blogs.filter(category='B')
        context['conferences'] = blogs.filter(category='C')
        context['internationals'] = blogs.filter(category='I')
        context['blog_index_pages'] = BlogIndexPage.objects.live().all()
        return context

    class Meta:
        verbose_name = u'議題頁'


BlogIndexPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('title_en', classname='full title'),
    FieldPanel('intro', classname='full'),
    FieldPanel('intro_en', classname='full'),
    ImageChooserPanel('image'),
]

