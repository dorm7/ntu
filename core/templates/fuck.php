<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Language" content="en" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<title>TELESCOPE SUPPLIERS - SKY-WATCHER TELESCOPE</title>
<meta name="description"
	content="Telescope suppliers. Welcome to www.SkywatcherTelescope.net. In this site you will also find tips on choosing a telescope and observing the night sky." />
<meta name="keywords"
	content="telescope suppliers,telescope,skywatcher,sky watcher,sky-watcher,synscan,dobsonian,heq5,eq6,eq6 pro" />
<link rel="shortcut icon"
	href="http://www.skywatchertelescope.net/swtinc/favicon.ico">
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<link rel="stylesheet" type="text/css"
	href="http://www.skywatcher.com/swtinc/ShadowBox/shadowbox.css"><script
	type="text/javascript"
	src="http://www.skywatcher.com/swtinc/ShadowBox/shadowbox.js"></script><script
	type="text/javascript">Shadowbox.init();</script> <script
	language="javascript" src="general.js"></script> <script
	language="JavaScript1.2" type="text/javascript" src="mm_css_menu.js"></script>
<script>
//  if(window.location.host != "www.skywatcher.com") {
//  var a = "" + window.location;
//  window.location = a.replace(window.location.host, "www.skywatcher.com");
//  //window.location = "http://www.skywatcher.com";
//  }
//  if(document.referrer == '' && document.location.href == "http://www.skywatcher.com/swtinc/product.php?id=204&class1=1&class2=140")
//    window.location = "http://ca.skywatcher.com/_english/01_products/02_detail.php?sid=379";

</script>
<style type="text/css" media="screen">
@import url("stylesheet_homepage.css");

.popup {
	z-index: 1
}
</style>

<script language="JavaScript1.2" type="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_nbGroup(event, grpName) { //v6.0
var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])?args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) { img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr) for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

//-->
</script>

</head>

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0"
	leftmargin="2" rightmargin="0" bgcolor="#FFFFFF"
	style="background: #FFFFFF url('http://www.skywatchertelescope.net/swtimages/bg.jpg') top center fixed no-repeat;">
<div id="wrap">
<table border="0" width="760" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="71"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="104"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="33"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="15"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="79"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="11"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="97"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="31"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="73"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="82"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="17"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="48"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="87"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="12"></td>
		<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif'; ?>" alt=""
			border="0" height="1" width="1"></td>
	</tr>

	<tr>
		<td colspan="4"><a href="http://www.skywatchertelescope.net/"
			title="telescope suppliers"> <img name="logo"
			src="<?php echo DIR_WS_IMAGES.'logo.gif'; ?>" id="logo" alt="HOME"
			border="0"></a></td>
		<td colspan="10" align=right valign="bottom">
		<table border="0">
			<tr>

				<div id="google_translate_element" style="visibility: hidden;"></div>
				<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
</script>
				<script
					src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				<script
					src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
				<script src="/jqzoom_ev1.0.1/js/jqzoom.pack.1.0.1.js"></script>
				<script>
// from http://jehiah.cz/archive/firing-javascript-events-properly
function fireEvent(element,event){
    if (document.createEventObject){
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }
    else{
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
}
 function translate(lan)
 {
 	var zz = $(".goog-te-combo")[0];
 	for(var i =0; i< zz.options.length; i++) {
		if (zz.options[i].value == lan) {
			zz.selectedIndex= i;
			fireEvent(zz, 'change');
			return;
		}
 	}
 }
// $(document).ready(function(){
// 	var options = {
//	    zoomWidth: 500,
//	    zoomHeight: 500,
//        xOffset: 10,
//        yOffset: 0,
//
//        };
//
//	$('.MYZOOM').jqzoom();
//});
 </script>
				<div><a href="javascript:translate('fr');" border="0"><img
					border="0" src="/swtnavimages/France.gif" title="French"
					style="margin: 1px 0px 1px 0px"></a> <a
					href="javascript:translate('de');" border="0"><img border="0"
					src="/swtnavimages/Germany.gif" title="German"
					style="margin: 1px 0px 1px 0px"></a> <a
					href="javascript:translate('it');" border="0"><img border="0"
					src="/swtnavimages/Italy.gif" title="Italian"
					style="margin: 1px 0px 1px 0px"></a> <a
					href="javascript:translate('ru');" border="0"><img border="0"
					src="/swtnavimages/Russian.gif" title="Russian"
					style="margin: 1px 0px 1px 0px"></a> <a
					href="javascript:translate('es');" border="0"><img border="0"
					src="/swtnavimages/Spain.gif" title="Spanish"
					style="margin: 1px 0px 1px 0px"></a> <a
					href="javascript:translate('af');" border="0"><img border="0"
					src="/swtnavimages/more.gif" title="Others"
					style="margin: 1px 0px 1px 0px"></a></div>
					<?php
					print "<table border=\"0\"><tr><td><form name=\"form_search01\" action=\"search.php\" Method=\"POST\"> ";
					print "<INPUT TYPE=hidden name=\"sendct001\" Value=1 /> ";
					print "<INPUT TYPE=\"text\" name=\"search01\" SIZE=25 value=\"\"></td><td>";
					print " <INPUT TYPE=\"image\" src=\"".DIR_WS_IMAGES."search.gif\" value=\"true\"> </form></td></tr></table>";
					//print " <INPUT TYPE=\"SUBMIT\" value=\"Product Search\"> </form>";
					?>
				<td><img src="product_files/spacer.gif" alt="" border="0"
					height="78" width="1"></td>
			</tr>

			<tr>
				<td colspan="15"><img src="<?php echo DIR_WS_IMAGES.'line.gif'; ?>"
					border="0" height="1" width="760"></td>
			</tr>

			<tr>
				<td><a href="http://www.skywatchertelescope.net/" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c1','<?php echo DIR_WS_IMAGES.'menu01a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu01.gif'; ?>',1);"><img
					name="menu01" src="<?php echo DIR_WS_IMAGES.'menu01.gif';?>"
					id="Interfacedesign_telescopes_productdetails_final_r2_c1" alt=""
					border="0" height="21" width="71"></a></td>
				<td><a href="product.php" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c2','<?php echo DIR_WS_IMAGES.'menu02a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu02.gif'; ?>',1);MM_menuShowMenu('MMMenuContainer0201092332_0', 'MMMenu0201092332_0',50,22,'menu02');"><img
					name="menu02" src="<?php echo DIR_WS_IMAGES.'menu02.gif';?>"
					id="menu02" alt="" border="0" height="21" width="104"></a></td>
				<td colspan="4"><a href="knowledge_base.php" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c3','<?php echo DIR_WS_IMAGES.'menu03a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu03.gif'; ?>',1);"><img
					name="menu03" src="<?php echo DIR_WS_IMAGES.'menu03.gif';?>"
					id="Interfacedesign_telescopes_productdetails_final_r2_c3" alt=""
					border="0" height="21" width="138"></a></td>
				<td colspan="2"><a href="gallery.php" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c7','<?php echo DIR_WS_IMAGES.'menu04a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu04.gif'; ?>',1);"><img
					name="menu04" src="<?php echo DIR_WS_IMAGES.'menu04.gif';?>"
					id="Interfacedesign_telescopes_productdetails_final_r2_c7" alt=""
					border="0" height="21" width="128"></a></td>
				<td colspan="2"><a href="customer_support.php" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c9','<?php echo DIR_WS_IMAGES.'menu05a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu05.gif'; ?>',1);"><img
					name="menu05" src="<?php echo DIR_WS_IMAGES.'menu05.gif';?>"
					id="Interfacedesign_telescopes_productdetails_final_r2_c9" alt=""
					border="0" height="21" width="155"></a></td>
				<td colspan="4"><a href="fun.php" target="_self"
					onmouseout="MM_nbGroup('out');"
					onmouseover="MM_nbGroup('over','Interfacedesign_telescopes_productdetails_final_r2_c11','<?php echo DIR_WS_IMAGES.'menu06a.gif'; ?>','<?php echo DIR_WS_IMAGES.'menu06.gif'; ?>',1);"><img
					name="menu06" src="<?php echo DIR_WS_IMAGES.'menu06.gif';?>"
					id="Interfacedesign_telescopes_productdetails_final_r2_c11" alt=""
					border="0" height="21" width="164"></a></td>
				<td><img src="<?php echo DIR_WS_IMAGES.'spacer.gif';?>" alt=""
					border="0" height="21" width="1"></td>
			</tr>


			<tr>
				<td colspan="15"><img src="<?php echo DIR_WS_IMAGES.'line.gif'; ?>"
					border="0" height="1" width="760"></td>
			</tr>

		</table>

		<?php

			$link = mysql_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
			if (!$link) { die('Could not connect: ' . mysql_error());	}

			mysql_select_db(DB_DATABASE) or die('Could not select database');


?> <?php
require('popup.php');
?>