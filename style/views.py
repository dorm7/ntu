__author__ = 'tim'
from django_kss.views import AutoStyleGuideView
from django.utils.translation import ugettext as _


class StyleGuideView(AutoStyleGuideView):
    template_name = 'ntu_guide.html'

