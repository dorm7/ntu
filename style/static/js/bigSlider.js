/* global jQuery*/
/* global console*/
(function(jQuery) {
    'use strict';

    jQuery.fn.bigSlider = function () {
        return this.each(function () {
            var $root = jQuery(this);

            var slider = {
                init:function($root){
                    slider.$root = $root;
                    slider.$container = slider.$root.find('.BigSlider-carouselInner');
                    slider.$prevButton = slider.$root.find('[data-slide=prev]');
                    slider.$nextButton = slider.$root.find('[data-slide=next]');
                    slider.setup();
                },
                setup:function(){
                    slider.$nextButton.click(slider.handleNext);
                    slider.$prevButton.click(slider.handlePrevious);
                },
                _getVisualWidth: function(){
                    return slider.$root.width();
                },
                handlePrevious: function(){
                    console.log('prev');
                    slider.$container.animate({
                        scrollLeft: "-=" + slider._getVisualWidth()
                    });
                },
                handleNext: function(){
                    console.log('next');
                    slider.$container.animate({
                        scrollLeft: "+=" + slider._getVisualWidth()
                    });
                }
            };

            slider.init($root);

            return this;
        });
    };
})(jQuery);
